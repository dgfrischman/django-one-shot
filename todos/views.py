from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

# Create your views here.

def show_todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list= TodoList.objects.get(id=id)
    context = {
        "todo_list": todo_list
    }
    return render(request, "todos/detail.html", context)

def create_todo_list(request):
  if request.method == "POST":
    form = TodoForm(request.POST)
    if form.is_valid():
      form = form.save()
      return redirect("todo_list_detail", id=form.id)

  else:
    form = TodoForm()

  context = {
    "form": form
  }

  return render(request, "todos/create.html", context)

def todo_list_update(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    form = TodoForm(request.POST, instance=model_instance)
    if form.is_valid():
      form = form.save()
      return redirect("todo_list_detail", id=form.id)
  else:
    form = TodoForm(instance=model_instance)

  context = {
    "form": form
  }

  return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")

def todo_item_create(request):
  if request.method == "POST":
    form = ItemForm(request.POST)
    if form.is_valid():
      item = form.save()
      return redirect("todo_list_detail", id=item.list.id)

  else:
    form = ItemForm()

  context = {
    "form": form
  }

  return render(request, "todos/items/create.html", context)

def todo_item_update(request, id):
  model_instance = TodoItem.objects.get(id=id)
  if request.method == "POST":
    form = ItemForm(request.POST, instance=model_instance)
    if form.is_valid():
      item = form.save()
      return redirect("todo_list_detail", id=item.list.id)
  else:
    form = ItemForm(instance=model_instance)

  context = {
    "form": form
  }

  return render(request, "todos/edit.html", context)
